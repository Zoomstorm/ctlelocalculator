package com.planed.ctlEloCalculator;

import com.planed.ctlEloCalculator.data.*;
import com.planed.ctlEloCalculator.services.*;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class LiquipediaUploaderTest {
    private static final Logger logger = LoggerFactory.getLogger(LiquipediaUploaderTest.class);

    @Autowired
    private LiquipediaImporter liquipediaImporter;
    @MockBean
    private LiquipediaReader liquipediaReader;
    @MockBean
    private LiquipediaUpdateExecutor liquipediaUpdateExecutor;
    @Autowired
    private LiquipediaMainTableExporter liquipediaMainTableExporter;
    @Autowired
    private LiquipediaUserPageExporter liquipediaUserPageExporter;
    @Autowired
    private LiquipediaTeamPageExporter liquipediaTeamPageExporter;

    @Before
    public void setUp() throws IOException {
        for (int i = 4; i < 22; i++) {
            if (i == 11) {
                doReturn(Arrays.asList(FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + ".html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Group_Stage_1.html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Group_Stage_2.html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Playoffs.html"))))
                        .when(liquipediaReader).readSeasonFiles(i);
            } else {
                doReturn(Arrays.asList(FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + ".html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Group_Stage.html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Playoffs.html"))))
                        .when(liquipediaReader).readSeasonFiles(i);
            }
        }
    }

    @Test
    public void uploadCompleteTable() {
        final ChoboLeagueData data = liquipediaImporter.importData();
//        final ChoboLeagueData data = liquipediaImporter.importData(4, 5, 6, 7, 8, 9);

        liquipediaMainTableExporter.uploadTable("CtlTable2", data);
    }


    @Test
    public void uploadTeamPage() {
        final ChoboLeagueData data = liquipediaImporter.importData();
//        final ChoboLeagueData data = liquipediaImporter.importData(4, 5, 6, 7, 8, 9);

        data.getTeams().values().stream()
                .forEach(team -> liquipediaTeamPageExporter.uploadTeam(team, data));
//        liquipediaUserPageExporter.uploadPlayer(data.getPlayers().get("fustup"));
    }

    @Test
    public void uploadPlayerPage() {
        final ChoboLeagueData data = liquipediaImporter.importData();

        liquipediaMainTableExporter.uploadTable("CtlTable", data);

        final List<Player> values = new ArrayList<>(data.getPlayers().values());
        Collections.shuffle(values);
        int i = 0;
        long t1 = System.currentTimeMillis();
        for (Player p : values) {
            liquipediaUserPageExporter.uploadPlayer(p);
            if ((i++ % 10) == 0) {
                Duration elapsed = Duration.ofMillis(System.currentTimeMillis() - t1);
                double fraction = i / (double) values.size();
                double msPerArticle = (elapsed.toMillis() / ((double) i));
                Duration remain = Duration.ofMillis((long) ((values.size() - i) * msPerArticle));
                logger.info("Checked " + i + " items of " + values.size() + ", being " + fraction * 100 + "%. Elapsed: " + elapsed + ", remaining: " + remain);
            }
        }
    }
}
