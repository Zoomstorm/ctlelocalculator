package com.planed.ctlEloCalculator;

import com.planed.ctlEloCalculator.data.*;
import com.planed.ctlEloCalculator.services.LiquipediaImporter;
import com.planed.ctlEloCalculator.services.LiquipediaReader;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class LiquipediaImporterTest {
    @InjectMocks
    private LiquipediaImporter liquipediaImporter;
    @Mock
    private LiquipediaReader liquipediaReader;

    @Before
    public void setUp() throws IOException {
        for (int i = 9; i < 22; i++) {
            if (i == 11) {
                doReturn(Arrays.asList(FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Group_Stage_1.html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Group_Stage_2.html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Playoffs.html"))))
                        .when(liquipediaReader).readSeasonFiles(i);
            } else {
                doReturn(Arrays.asList(FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Group_Stage.html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Playoffs.html"))))
                        .when(liquipediaReader).readSeasonFiles(i);
            }
        }
    }

    @Test
    public void shouldReturnSeasonFile() {
        final ChoboLeagueData choboLeagueData = liquipediaImporter.importData();

        // Regular Matches
        assertThat(choboLeagueData.getMatches()).contains(match(League.GOLD, "Tropical", Race.TERRAN, "Turtyo", Race.TERRAN, 0, 1, 1, 1171.1335255669505, 1210.0, "Taste The Bacon", "Alpha X"));
        assertThat(choboLeagueData.getMatches()).contains(match(League.DIAMOND, "jayhxmo", Race.PROTOSS, "PSosa", Race.ZERG, 0, 1, 6, 1199.4890235059836, 1201.6992060330772, "Daily Life", "All-Inspiration"));
        // Ace Match
        assertThat(choboLeagueData.getMatches()).contains(match(League.MASTER, "Siegfried", Race.TERRAN, "Vedeynevin", Race.ZERG, 0, 2, 7, 1237.1907258584754, 1231.0505885662872, "All-Inspiration", "Formless Bearsloths"));
        // Playoff Match
        assertThat(choboLeagueData.getMatches()).contains(match(League.DIAMOND23, "ggallin", Race.TERRAN, "TheFetus", Race.PROTOSS, 0, 1, 8, 1190.2161016098064, 1207.073935308818, "Cutie Patootie", "Born Gosu"));
    }

    @Test
    public void shouldMergeAceMatch() {
        final ChoboLeagueData choboLeagueData = liquipediaImporter.importData();
        assertThat(choboLeagueData.getPlayers().get("roxas").getMatches()).hasSize(6);
    }

    @Test
    public void shouldCorrectlyParseWalkovers() {
        final ChoboLeagueData choboLeagueData = liquipediaImporter.importData();

        // No Playername
        assertThat(choboLeagueData.getMatches()).contains(Match.builder()
                .league(League.GOLD)
                .season(10)
                .week(6)
                .player1Score(0)
                .player2Score(1)
                .player1Wins(false)
                .player2Wins(true)
                .player1(null)
                .player2(Player.builder()
                        .name("havOc")
                        .race(Race.ZERG)
                        .league(League.GOLD)
                        .team(Team.builder()
                                .name("House of Renegades")
                                .build())
                        .elo(1209.71225631668)
                        .build())
                .isWalkover(true)
                .build());

        // Regular Walkover
        final Match match = match(League.PLATINUM, "Notorious", Race.PROTOSS, "Cgrij", Race.TERRAN, 1, 0, 6, 1161.9695111742683, 1247.7273132606763, "The Art of Warfare", "House of Renegades");
        match.setWalkover(true);
        match.setSeason(10);
        assertThat(choboLeagueData.getMatches()).contains(match);

    }

    private Match match(League league, String n1, Race r1, String n2, Race r2, int p1Score, int p2Score, int week, double p1Elo, double p2Elo, String team1Name, String team2Name) {
        final Match match = Match.builder()
                .league(league)
                .season(21)
                .week(week)
                .player1Score(p1Score)
                .player2Score(p2Score)
                .player1(Player.builder()
                        .name(n1)
                        .race(r1)
                        .league(league)
                        .elo(p1Elo)
                        .team(Team.builder()
                                .name(team1Name)
                                .build())
                        .build())
                .player2(Player.builder()
                        .name(n2)
                        .race(r2)
                        .league(league)
                        .elo(p2Elo)
                        .team(Team.builder()
                                .name(team2Name)
                                .build())
                        .build())
                .build();
        match.calculateWinner();
        return match;
    }
}
