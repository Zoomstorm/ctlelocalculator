package com.planed.ctlEloCalculator;

import com.planed.ctlEloCalculator.data.ChoboLeagueData;
import com.planed.ctlEloCalculator.services.LiquipediaImporter;
import com.planed.ctlEloCalculator.services.LiquipediaReader;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class TestLiquipediaImportSpring {
    @Autowired
    private LiquipediaImporter liquipediaImporter;
    @Autowired
    private LiquipediaReader liquipediaReader;

    @Before
    public void setUp() throws IOException {
        /*for (int i = 4; i < 22; i++) {
            if (i == 11) {
                doReturn(Arrays.asList(FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + ".html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Group_Stage_1.html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Group_Stage_2.html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Playoffs.html"))))
                        .when(liquipediaReader).readSeasonFiles(i);
            } else {
                doReturn(Arrays.asList(FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + ".html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Group_Stage.html")),
                        FileUtils.readFileToString(new File("src/test/resources/liquipedia/S" + i + "_Playoffs.html"))))
                        .when(liquipediaReader).readSeasonFiles(i);
            }
        }*/
    }

    @Test
    public void importAll() {
        final ChoboLeagueData choboLeagueData = liquipediaImporter.importData();
        System.out.println(choboLeagueData.getPlayers().get("fustup"));
    }
}
