package com.planed.ctlEloCalculator.executor;

import com.planed.ctlEloCalculator.data.*;
import lombok.Builder;
import lombok.Data;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class LiquipediaImporterExecutor {
    private static final Logger logger = LoggerFactory.getLogger(LiquipediaImporterExecutor.class);
    private static final String WINNER_TD_STYLE = "background-color:#c0f0c0";
    private static final double INITIAL_ELO = 1200.;

    private Map<String, String> wrongToRightNameMap = new HashMap() {{
        put("TGkewlbeanz".toLowerCase(), "ShineKO".toLowerCase());
        put("SavageSam".toLowerCase(), "HuGoAhab".toLowerCase());
        put("notDroid".toLowerCase(), "SuperNova".toLowerCase());
        put("Gnarlwhal".toLowerCase(), "Gnarwhal".toLowerCase());
        put("Hamza".toLowerCase(), "HmzaBshr".toLowerCase());
        put("SCIIBMRater".toLowerCase(), "TechFour".toLowerCase());
        put("Nerdicus".toLowerCase(), "xKyouma".toLowerCase());
        put("Intution".toLowerCase(), "Intuition".toLowerCase());
        put("Ala5ar".toLowerCase(), "Alasar".toLowerCase());
        put("Thantatos".toLowerCase(), "Thanatos".toLowerCase());
        put("oddity".toLowerCase(), "OddHonor".toLowerCase());
        put("Cobalt".toLowerCase(), "Cobaltt".toLowerCase());
        put("Xela".toLowerCase(), "Xeladragn".toLowerCase());
        put("Gnarlwhal".toLowerCase(), "Gnarwhal".toLowerCase());
        put("Leafhealfer".toLowerCase(), "Gnarwhal".toLowerCase());
        put("Itachi".toLowerCase(), "ItachiUchiha".toLowerCase());
        put("Darth".toLowerCase(), "DarthKing".toLowerCase());
        put("tn5421".toLowerCase(), "Harumi".toLowerCase());
        put("Terranitup".toLowerCase(), "CyRo".toLowerCase());
        put("Jaxmus".toLowerCase(), "Jaximus".toLowerCase());
        put("Tinysnowman".toLowerCase(), "Tinysnowmen".toLowerCase());
        put("Colttarren".toLowerCase(), "Coltarren".toLowerCase());
        put("saucegxd".toLowerCase(), "SauCeBoSS".toLowerCase());
        put("SauCeKing".toLowerCase(), "SauCeBoSS".toLowerCase());
        put("Wobs".toLowerCase(), "Wobsitopian".toLowerCase());
        put("Otsdava".toLowerCase(), "Otsdarva".toLowerCase());
        put("nickohonski".toLowerCase(), "kohonski".toLowerCase());
        put("TossnoKSill".toLowerCase(), "TossNoSkill".toLowerCase());
        put("yaygerflavor".toLowerCase(), "Yagerflavor".toLowerCase());
        put("KiiA".toLowerCase(), "KiiiA".toLowerCase());
        put("KiiiiA".toLowerCase(), "KiiiA".toLowerCase());
        put("KìììÂ".toLowerCase(), "KiiiA".toLowerCase());
        put("SavageSam".toLowerCase(), "HuGoAhab".toLowerCase());
        put("HuGo".toLowerCase(), "HuGoAhab".toLowerCase());
        put("Yukingagato".toLowerCase(), "Yukinagato".toLowerCase());
        put("Yukingato".toLowerCase(), "Yukinagato".toLowerCase());
        put("AshKetchup".toLowerCase(), "AshKetchupp".toLowerCase());
        put("KiaphTheNoGG".toLowerCase(), "Kiaph".toLowerCase());
        put("oddity".toLowerCase(), "OddHonor".toLowerCase());
        put("HonoR".toLowerCase(), "OddHonor".toLowerCase());
        put("GingerHobbit".toLowerCase(), "RikkiRose".toLowerCase());
        put("Blaze".toLowerCase(), "Blazejwa".toLowerCase());
        put("m0rzh".toLowerCase(), "Timofey".toLowerCase());
        put("AshenNA".toLowerCase(), "AsheN".toLowerCase());
        put("Miyamori".toLowerCase(), "Kori".toLowerCase());
        put("iaintshook".toLowerCase(), "Shooknasty".toLowerCase());
        put("iaintshook".toLowerCase(), "Shooknasty".toLowerCase());
        put("nano        ".trim().toLowerCase(), "Nano-Swiftwind".trim().toLowerCase());
        put("Nano            ".trim().toLowerCase(), "Nanowa      ".trim().toLowerCase());
        put("AustrainOak     ".trim().toLowerCase(), "AustrianOak             ".trim().toLowerCase());
        put("TGkewlbeanz     ".trim().toLowerCase(), "ShineKO             ".trim().toLowerCase());
        put("SavageSam   ".trim().toLowerCase(), "HuGoAhab            ".trim().toLowerCase());
        put("HuGo    ".trim().toLowerCase(), "HuGoAhab            ".trim().toLowerCase());
        put("notDroid    ".trim().toLowerCase(), "SuperNova           ".trim().toLowerCase());
        put("Gnarlwhal   ".trim().toLowerCase(), "Gnarwhal            ".trim().toLowerCase());
        put("Leafhealfer     ".trim().toLowerCase(), "Gnarwhal            ".trim().toLowerCase());
        put("Hamza   ".trim().toLowerCase(), "HmzaBshr            ".trim().toLowerCase());
        put("SCIIBMRaterd    ".trim().toLowerCase(), "TechFour            ".trim().toLowerCase());
        put("Table   ".trim().toLowerCase(), "TechFour            ".trim().toLowerCase());
        put("Nerdicus    ".trim().toLowerCase(), "xKyouma             ".trim().toLowerCase());
        put("Intution    ".trim().toLowerCase(), "Intuition           ".trim().toLowerCase());
        put("Ala5ar  ".trim().toLowerCase(), "Alasar          ".trim().toLowerCase());
        put("Thantatos   ".trim().toLowerCase(), "Thanatos            ".trim().toLowerCase());
        put("oddity  ".trim().toLowerCase(), "OddHonor            ".trim().toLowerCase());
        put("HonoR   ".trim().toLowerCase(), "OddHonor            ".trim().toLowerCase());
        put("Xela    ".trim().toLowerCase(), "Xeladragn           ".trim().toLowerCase());
        put("Itachi  ".trim().toLowerCase(), "ItachiUchiha            ".trim().toLowerCase());
        put("Darth   ".trim().toLowerCase(), "DarthKing           ".trim().toLowerCase());
        put("tn5421  ".trim().toLowerCase(), "Harumi          ".trim().toLowerCase());
        put("Terranitup  ".trim().toLowerCase(), "CyRo            ".trim().toLowerCase());
        put("Jaxmus  ".trim().toLowerCase(), "Jaximus             ".trim().toLowerCase());
        put("Tinysnowman     ".trim().toLowerCase(), "Tinysnowmen             ".trim().toLowerCase());
        put("Colttarren  ".trim().toLowerCase(), "Coltarren           ".trim().toLowerCase());
        put("Colattarren".trim().toLowerCase(), "Coltarren           ".trim().toLowerCase());
        put("Colttaren".trim().toLowerCase(), "Coltarren           ".trim().toLowerCase());
        put("Wobs    ".trim().toLowerCase(), "Wobsitopian             ".trim().toLowerCase());
        put("Otsdava     ".trim().toLowerCase(), "Otsdarva            ".trim().toLowerCase());
        put("nickohonski     ".trim().toLowerCase(), "kohonski            ".trim().toLowerCase());
        put("TossnoKSill     ".trim().toLowerCase(), "TossNoSkill             ".trim().toLowerCase());
        put("yaygerflavor    ".trim().toLowerCase(), "Yagerflavor             ".trim().toLowerCase());
        put("Yukingagato     ".trim().toLowerCase(), "Yukinagato          ".trim().toLowerCase());
        put("Yukingato   ".trim().toLowerCase(), "Yukinagato          ".trim().toLowerCase());
        put("AshKetchup  ".trim().toLowerCase(), "AshKetchupp             ".trim().toLowerCase());
        put("KiaphTheNoGG    ".trim().toLowerCase(), "Kiaph           ".trim().toLowerCase());
        put("GingerHobbit    ".trim().toLowerCase(), "RikkiRose           ".trim().toLowerCase());
        put("FreePuddle  ".trim().toLowerCase(), "Puddle          ".trim().toLowerCase());
        put("ultralisk   ".trim().toLowerCase(), "Damthatsbig         ".trim().toLowerCase());
        put("m0rzh   ".trim().toLowerCase(), "Timofey             ".trim().toLowerCase());
        put("Miyamori    ".trim().toLowerCase(), "Kori            ".trim().toLowerCase());
        put("AshenNA     ".trim().toLowerCase(), "AsheN           ".trim().toLowerCase());

        put("Defenestrata".toLowerCase(), "Defenstrator".toLowerCase());
        put("Defenstrata".toLowerCase(), "Defenstrator".toLowerCase());
        put("Demostehenes".toLowerCase(), "Demosthenes".toLowerCase());
        put("Don Jimbo".toLowerCase(), "Donjimbo".toLowerCase());
        put("DraxThDstry".toLowerCase(), "DraxThDstryr".toLowerCase());
        put("DraxThDstyr".toLowerCase(), "DraxThDstryr".toLowerCase());
        put("DraxTheDstyr".toLowerCase(), "DraxThDstryr".toLowerCase());
        put("Draz0000".toLowerCase(), "Draz".toLowerCase());
        put("ExtraenouS".toLowerCase(), "ExtraneouS".toLowerCase());
        put("Hassenderr".toLowerCase(), "hasenderr".toLowerCase());
        put("Holdean".toLowerCase(), "Holdaen".toLowerCase());
        put("Holdean".toLowerCase(), "Holdaen".toLowerCase());
        put("Iainshook".toLowerCase(), "iaintshook".toLowerCase());
        put("Iainshook".toLowerCase(), "iaintshook".toLowerCase());
        put("Inglourious".toLowerCase(), "Inglorious".toLowerCase());
        put("Jaffars".toLowerCase(), "Jaffers".toLowerCase());
        put("Jambon Rose".toLowerCase(), "JambonRose".toLowerCase());
        put("Start".toLowerCase(), "Starting".toLowerCase());
        put("tsDjKniteX".toLowerCase(), "tSnDjKniteX".toLowerCase());
        put("jpegNT".toLowerCase(), "jpegNY".toLowerCase());
        put("pointdexter".toLowerCase(), "Poindexter".toLowerCase());
        put("Boss TerraN".toLowerCase(), "BossTerraN".toLowerCase());
        put("pointdexter".toLowerCase(), "BossTerraN".toLowerCase());
        put("cHrono0".toLowerCase(), "cHron0".toLowerCase());
        put("Crypts".toLowerCase(), "Cryptys".toLowerCase());
        put("Damphotons".toLowerCase(), "DamnPhotons".toLowerCase());
        put("Droppinbodies".toLowerCase(), "Dropinbodies".toLowerCase());
        put("drumstix687".toLowerCase(), "drumstix678".toLowerCase());
        put("drumstix".toLowerCase(), "drumstix678".toLowerCase());
        put("Flanders992".toLowerCase(), "Flanders".toLowerCase());
        put("Flanders992".toLowerCase(), "Flanders".toLowerCase());
        put("ghreinlush".toLowerCase(), "ghrelinrush".toLowerCase());
        put("IllicitFoot6".toLowerCase(), "IlliciIllicitFoot6".toLowerCase());
        put("Illicifoot".toLowerCase(), "IlliciIllicitFoot6".toLowerCase());
        put("Illicifoot".toLowerCase(), "IlliciIllicitFoot6".toLowerCase());
        put("Leverager".toLowerCase(), "LeveRage".toLowerCase());
        put("MadPharoh".toLowerCase(), "MadPharaoh".toLowerCase());
        put("MeemoSan".toLowerCase(), "Meemosaan".toLowerCase());
        put("MemmoSan".toLowerCase(), "Meemosaan".toLowerCase());
        put("Michloz".toLowerCase(), "Michlolz".toLowerCase());
        put("Nano - Swiftwind".toLowerCase(), "nano".toLowerCase());
        put("neosufer".toLowerCase(), "Neosurfer".toLowerCase());
        put("PlzNrfTaren".toLowerCase(), "PlzNurfTaren".toLowerCase());
        put("PoTaToChipS".toLowerCase(), "PoTaToeChipS".toLowerCase());
        put("Shotland".toLowerCase(), "Shortland".toLowerCase());
        put("TUX".toLowerCase(), "TUXerino".toLowerCase());
        put("Valaa".toLowerCase(), "Vaala".toLowerCase());
        put("Zerging".toLowerCase(), "Zergring".toLowerCase());
        put("Bananekeks".toLowerCase(), "Bananenkeks".toLowerCase());
        put("Beardie88".toLowerCase(), "Beardie".toLowerCase());
        put("BlueEyeRy".toLowerCase(), "BlueEye".toLowerCase());
        put("bmwAssasin".toLowerCase(), "bmwAssassin".toLowerCase());
        put("bmwAssasin".toLowerCase(), "bmwAssassin".toLowerCase());
        put("Cactus".toLowerCase(), "Cactusman".toLowerCase());
        put("CrimsomRain".toLowerCase(), "CrimsonRain".toLowerCase());
        put("CutUrRibon".toLowerCase(), "CutUrRibbon".toLowerCase());
        put("DrEuclidaen".toLowerCase(), "DrEuclidean".toLowerCase());
        put("Fallen Angel".toLowerCase(), "fallenAngel".toLowerCase());
        put("fallenAangel".toLowerCase(), "fallenAngel".toLowerCase());
        put("Fizen".toLowerCase(), "Fizben".toLowerCase());
        put("Good Target".toLowerCase(), "GoodTarget".toLowerCase());
        put("Fizen".toLowerCase(), "GoodTarget".toLowerCase());
        put("Krawton".toLowerCase(), "Krawaton".toLowerCase());
        put("Lumencyster".toLowerCase(), "Lumencryster".toLowerCase());
        put("NACI".toLowerCase(), "NaCl".toLowerCase());
        put("oso1590".toLowerCase(), "oso".toLowerCase());
        put("Razgriz".toLowerCase(), "Raz".toLowerCase());
        put("Razzle".toLowerCase(), "Raz".toLowerCase());
        put("Roflmwafflez".toLowerCase(), "RoflWafflez".toLowerCase());
        put("Roflmaffle".toLowerCase(), "RoflWafflez".toLowerCase());
        put("Shifttix".toLowerCase(), "Shiftix".toLowerCase());
        put("SnakeDocotor".toLowerCase(), "SnakeDoctor".toLowerCase());
        put("Solace".toLowerCase(), "Solasce".toLowerCase());

        put("SalSal#2666".toLowerCase(), "SalSal".toLowerCase());
        put("Commando#1321".toLowerCase(), "Commando".toLowerCase());
        put("SGMisery#1889".toLowerCase(), "SGMisery".toLowerCase());
        put("arthisios/grixis".toLowerCase(), "arthisios".toLowerCase());
    }};

    private List<String> htmlData;
    private int season;
    private Map<String, Player> playerMap;
    private Map<String, Team> teamMap;
    private Map<String, Sc2Map> maps;
    private List<Match> matches;
    private List<String> teamsFinalPlacementTable;
    private Map<Integer, Boolean> seasonFinishedTable;

    @Builder
    public LiquipediaImporterExecutor(List<String> htmlData, int season, ChoboLeagueData choboLeagueData) {
        this.htmlData = htmlData;
        this.season = season;
        this.playerMap = choboLeagueData.getPlayers();
        this.matches = choboLeagueData.getMatches();
        this.teamMap = choboLeagueData.getTeams();
        this.maps = choboLeagueData.getMaps();
        this.seasonFinishedTable = choboLeagueData.getSeasonFinishedTable();
    }

    public ChoboLeagueData execute() {
        logger.info("Starting parsing season " + season + "...");

        int weekOffset = 0;
        for (String data : htmlData) {
            weekOffset += parsePage(data, weekOffset);
        }

        if (teamsFinalPlacementTable == null) {
            throw new RuntimeException("Could not find placement table!");
        }

        awardTeamHonors();
        awardPlayerHonors();

        seasonFinishedTable.put(season, !teamsFinalPlacementTable.isEmpty());

        return ChoboLeagueData.builder()
                .matches(matches)
                .players(playerMap)
                .teams(teamMap)
                .maps(maps)
                .seasonFinishedTable(seasonFinishedTable)
                .build();
    }

    private void awardTeamHonors() {
        if (teamsFinalPlacementTable.size() >= 4) {
            awardHonorToAllPlayersInTeam(getTeamByPlacement(1), HonorKind.FIRST_PLACE_IN_SEASON);
            awardHonorToAllPlayersInTeam(getTeamByPlacement(2), HonorKind.SECOND_PLACE_IN_SEASON);
            awardHonorToAllPlayersInTeam(getTeamByPlacement(3), HonorKind.THIRD_PLACE_IN_SEASON);

            int rank = 1;
            for (String teamName : teamsFinalPlacementTable) {
                final int fuckFinal = rank;
                teamMap.values().stream()
                        .filter(team -> team.getIconUrl().equals(teamName))
                        .findAny().ifPresent(team -> team.getSeasonPlacements().put(season, fuckFinal));
                rank++;
            }
        } else {
            logger.warn("Could not award Team Placement Honors for Season " + season);
        }
    }

    private void awardHonorToAllPlayersInTeam(Team team, HonorKind honorKind) {
        playerMap.values().stream()
                .filter(player -> player.getTeam().equals(team))
                .filter(player -> player.getMatches().stream()
                        .filter(match -> match.getSeason() == season)
                        .findAny().isPresent())
                .forEach(player -> player.addHonor(PlayerSeasonHonor.builder()
                        .season(season)
                        .kind(honorKind)
                        .build()));
    }

    private Team getTeamByPlacement(int placement) {
        return teamMap.values().stream()
                .filter(team -> team.getIconUrl().equals(teamsFinalPlacementTable.get(placement - 1)))
                .findAny().get();
    }

    private void awardPlayerHonors() {
        playerMap.values().stream()
                .filter(player -> player.getMatches().stream()
                        .filter(match -> match.getSeason() == season)
                        .filter(match -> !match.isPlayoffMatch())
                        .filter(match -> match.didPlayerWin(player))
                        .count() >= 5)
                .forEach(player -> player.addHonor(PlayerSeasonHonor.builder()
                        .kind(HonorKind.FIVE_SET_WINS_IN_SEASON)
                        .season(season)
                        .build()));
    }

    private int parsePage(String html, int weekOffset) {
        final Document document = Jsoup.parse(html);

        if (documentIsOverviewPage(document)) {
            parseOverviewPage(document);
            return 0;
        }

        boolean isPlayoff = season != 11 ? weekOffset != 0 : weekOffset > 4;
        int week = 0;
        final List<Element> select = selectWeekElements(document);
        for (Element div : select) {
            week++;
            parseWeek(div, weekOffset + week, isPlayoff);
        }

        return week;
    }

    private void parseOverviewPage(Document document) {
        teamsFinalPlacementTable = document.select("table").stream()
                .filter(e -> e.attr("class").equals("wikitable"))
                .filter(e -> e.text().startsWith("Place Team") || e.text().startsWith("Standings"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Could not find placement table!"))
                .select("tr").stream()
                .filter(tr -> tr.hasAttr("style"))
                .map(tr -> tr.select("td").get(1))
                .filter(td -> !StringUtils.isEmpty(td.text()))
                .map(td -> td.select("img").get(0).attr("src"))
                .map(imgPath -> imgPath.substring(imgPath.lastIndexOf("/") + 1))
                .collect(Collectors.toList());
    }

    private boolean documentIsOverviewPage(Document document) {
        return document.select("h2").stream()
                .map(Element::text)
                .filter(str -> str.startsWith("Tournament Information"))
                .findAny().isPresent();
    }

    private List<Element> selectWeekElements(Document document) {
        if (season > 9) {
            return document.select("div.tabs-dynamic")
                    .select("div.tabs-content > div");
        } else if (season <= 5) {
            return document.select("div.mw-parser-output > div").stream()
                    .filter(div -> !div.hasAttr("class"))
                    .collect(Collectors.toList());
        } else {
            return document.select("div.tabs-container > div");
        }
    }

    private Integer parseWeek(Element div, int week, boolean isPlayoff) {
        div.select("div.template-box")
                .stream()
                .forEach(box -> parseTeamMatchSafe(box, week, isPlayoff));

        return week;
    }

    private void parseTeamMatchSafe(Element box, int week, boolean isPlayoff) {
        try {
            parseTeamMatch(box, week, isPlayoff);
        } catch (Exception e) {
            //logger.warn("Content: " + box.html());
            throw new RuntimeException("Error while parsing Season " + season + ", Week " + week, e);
        }
    }

    private void parseTeamMatch(Element box, int week, boolean isPlayoff) {
        final List<Element> teams =
                box.select("td.matchslot").stream()
                        .collect(Collectors.toList());

        if (teams.size() != 2) {
            logger.warn("Could not determine Team name! season=" + season + ", week=" + week + "");
            return;
        }

        if (teams.get(0).text().contains("All-Stars")) {
            return;
        }

        final List<Match> rawMatches = box.select("table.teammatch-games").select("tr").stream()
                .filter(row -> row.select("td").size() == 2)
                .map(row -> parseMatch(row, teams, week, isPlayoff))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        for (Match match : filterOutAceMatch(rawMatches)) {
            adjustEloForPlayers(match);
            adjustPointsForPlayers(match);

            if (match.getPlayer1() != null) {
                match.getPlayer1().getMatches().add(match);
            }
            if (match.getPlayer2() != null) {
                match.getPlayer2().getMatches().add(match);
            }

            this.matches.add(match);
        }
    }

    private void adjustPointsForPlayers(Match match) {
        if (match.getPlayer1() != null) {
            match.getPlayer1().addPoints(match.mapToPoints(match.getPlayer1()));
        }
        if (match.getPlayer2() != null) {
            match.getPlayer2().addPoints(match.mapToPoints(match.getPlayer2()));
        }
    }

    private void adjustEloForPlayers(Match match) {
        if (match.isWalkover()) {
            return;
        }

        match.getPlayer1().setElo(match.getPlayer1().getElo() + match.getEloAdjustmentForPlayer(match.getPlayer1()));
        match.getPlayer2().setElo(match.getPlayer2().getElo() + match.getEloAdjustmentForPlayer(match.getPlayer2()));
    }

    private List<Match> filterOutAceMatch(List<Match> rawMatches) {
        List<Match> result = new ArrayList<>();

        Match aceMatch = null;
        Match lastMatch = null;

        for (Match match : rawMatches) {
            if (lastMatch != null
                    && lastMatch.getPlayer1() != null
                    && lastMatch.getPlayer1().equals(match.getPlayer1())) {
                if (aceMatch == null) {
                    result.remove(lastMatch);
                    aceMatch = lastMatch;
                }
                aceMatch.addPlayer1Score(match.getPlayer1Score());
                aceMatch.addPlayer2Score(match.getPlayer2Score());
            } else {
                result.add(match);
            }
            lastMatch = match;
        }
        if (aceMatch != null) {
            result.add(aceMatch);
            aceMatch.calculateWinner();
            aceMatch.setAceMatch(true);
        }

        return result;
    }

    private Optional<Match> parseMatch(Element row, List<Element> teamNames, int week, boolean isPlayoff) {
        Elements tdList = row.select("td");
        Player player1 = findOrCreatePlayer(tdList.get(0), teamNames.get(0));
        Player player2 = findOrCreatePlayer(tdList.get(1), teamNames.get(1));
        if (player1 == null && player2 == null) {
            return Optional.empty();
        }
        final boolean player1Wins = tdList.get(0).attr("style").contains(WINNER_TD_STYLE);
        final boolean player2Wins = tdList.get(1).attr("style").contains(WINNER_TD_STYLE);
        final Match match = Match.builder()
                .week(week)
                .season(season)
                .league(getHigherLeague(player1, player2))
                .playoffMatch(isPlayoff)
                .map(findOrCreateMap(row.select("th")))

                .player1(player1)
                .player2(player2)

                .player1Team(findOrCreateTeam(teamNames.get(0)))
                .player2Team(findOrCreateTeam(teamNames.get(1)))

                .player1Wins(player1Wins)
                .player2Wins(player2Wins)

                .player1Score(player1Wins ? 1 : 0)
                .player2Score(player2Wins ? 1 : 0)
                .build();

        match.getMap().getMatches().add(match);

        if (player1 != null && player1.getLeague() == null) {
            player1.setLeague(match.getLeague());
        }
        if (player2 != null && player2.getLeague() == null) {
            player2.setLeague(match.getLeague());
        }

        if (player1 == null || player2 == null || !tdList.select("s").isEmpty()) {
            match.setWalkover(true);
        }

        if (match.isPlayer1Wins() && match.isPlayer2Wins()) {
            throw new RuntimeException("Match was won by both players: " + tdList.html());
        }

        return Optional.of(match);
    }

    private Sc2Map findOrCreateMap(Elements th) {
        String mapName = th.text().trim();
        if (maps.containsKey(mapName)) {
            return maps.get(mapName);
        } else {
            maps.put(mapName, Sc2Map.builder()
                    .mapName(mapName)
                    .build());

            return maps.get(mapName);
        }
    }

    private League getHigherLeague(Player player1, Player player2) {
        if (player1 == null || player1.getLeague() == null) {
            return player2.getLeague();
        }

        if (player2 == null || player2.getLeague() == null) {
            return player1.getLeague();
        }

        if (player1.getLeague().compareTo(player2.getLeague()) >= 0) {
            return player1.getLeague();
        } else {
            return player2.getLeague();
        }
    }

    private Player findOrCreatePlayer(Element element, Element team) {
        String playerName = element.text().trim().replace("*", "");
        if (StringUtils.isEmpty(playerName) || "No Player".equals(playerName)) {
            return null;
        }

        Optional<League> league = element.select("a").stream()
                .map(a -> a.attr("href"))
                .map(href -> League.parse(href))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst();

        Optional<Race> race = element.select("a").stream()
                .map(a -> a.attr("title"))
                .map(href -> Race.parse(href))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst();

        final Player player;
        if (playerMap.containsKey(normalizePlayerName(playerName))) {
            player = playerMap.get(normalizePlayerName(playerName));
        } else {
            player = Player.builder()
                    .name(playerName)
                    .elo(INITIAL_ELO)
                    .matches(new ArrayList<>())
                    .build();
            playerMap.put(normalizePlayerName(playerName), player);
        }

        if (race.isPresent()) {
            player.setRace(race.get());
        }
        if (league.isPresent()) {
            player.setLeague(league.get());
        }
        player.setTeam(findOrCreateTeam(team));
        return player;
    }

    private Team findOrCreateTeam(Element teamElement) {
        String teamName = teamElement.text().trim();
        if (teamMap.containsKey(teamName)) {
            return teamMap.get(teamName);
        } else {
            String[] parts = teamElement.select("img").get(0).attr("src").split("\\/");
            Team team = Team.builder()
                    .name(teamName)
                    .iconUrl(parts[parts.length - 1])
                    .build();
            teamMap.put(teamName, team);
            return team;
        }
    }

    private String normalizePlayerName(String playerName) {
        String name = playerName.toLowerCase();
        int loop = 0;
        while (wrongToRightNameMap.containsKey(name)) {
            name = wrongToRightNameMap.get(name);
            if (loop++ > 10) {
                throw new RuntimeException("Loop while processing " + name);
            }
        }
        return name;
    }
}
