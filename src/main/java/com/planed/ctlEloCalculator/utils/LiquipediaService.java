package com.planed.ctlEloCalculator.utils;

import com.planed.ctlEloCalculator.data.entities.ArticleEntity;
import com.planed.ctlEloCalculator.services.ArticleService;
import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.actions.util.ApiException;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Base64;

@Service
public class LiquipediaService {
    private static final Logger logger = LoggerFactory.getLogger(LiquipediaService.class);

    @Value("${liquipedia.username}")
    private String username;
    @Value("${liquipedia.password}")
    private String password;
    @Autowired
    private ArticleService articleService;

    public void writeArticleWithTextIfDiffsPresent(String path, String data) {
        writeSingleArticle(path, data);
    }

    private void writeSingleArticle(String path, String data) {
        final byte[] oldHash = getCachedArticleHash(path);
        final byte[] newHash = getArticleHash(data);
        if (Arrays.equals(oldHash, newHash)) {
            logger.info("No change detected in file '" + path + "'. Skipping write to Server.");
        } else {
            logger.info("Change detected in file '" + path + "'! Writing to Server...");
            MediaWikiBot wikiBot = new MediaWikiBot("https://liquipedia.net/starcraft2/");
            Article article = wikiBot.getArticle(path);
            article.setText(data.trim());
            saveRateLimited(article, wikiBot);
        }
    }

    private byte[] getCachedArticleHash(String path) {
        return articleService.getArticleByName(path)
                .map(ArticleEntity::getHashBase64)
                .map(base64Hash -> Base64.getDecoder().decode(base64Hash))
                .orElseGet(() -> retrieveAndSaveArticleHash(path));
    }

    private byte[] retrieveAndSaveArticleHash(String path) {
        try {
            Thread.sleep(2_000);
        } catch (InterruptedException e) {
        }
        MediaWikiBot wikiBot = new MediaWikiBot("https://liquipedia.net/starcraft2/");
        Article article = wikiBot.getArticle(path);
        byte[] articleHash = getArticleHash(article.getText());
        articleService.saveArticle(ArticleEntity.builder()
                .articleName(path)
                .hashValueLastUpdate(OffsetDateTime.now())
                .hashBase64(Base64.getEncoder().encodeToString(articleHash))
                .build());
        return articleHash;
    }

    private byte[] getArticleHash(String data) {
        return DigestUtils.md5Digest(data.trim().getBytes());
    }

    private void saveRateLimited(Article article, MediaWikiBot wikiBot) {
        while (true) {
            try {
                wikiBot.login(username, password);
                article.save();
                Thread.sleep(20_000);
                logger.info("Succesfully saved article '" + article.getTitle() + "'!");
                return;
            } catch (ApiException e) {
                if (!e.getMessage().contains("ratelimited VALUE")) {
                    throw e;
                } else {
                    logger.debug("Sleeping due to rate limitation while saving article '" + article.getTitle() + "'...");
                    try {
                        Thread.sleep(20_000);
                    } catch (InterruptedException e2) {
                    }
                }
            } catch (InterruptedException e) {
            }
        }
    }
}
