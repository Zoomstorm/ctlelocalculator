package com.planed.ctlEloCalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CtlEloCalculatorApplication {
    public static void main(String[] args) {
        SpringApplication.run(CtlEloCalculatorApplication.class, args);
    }
}
