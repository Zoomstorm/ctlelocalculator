package com.planed.ctlEloCalculator.data;

import java.util.Optional;

public enum League {
    BRONZE("BronzeMedium"),
    SILVER("SilverMedium"),
    GOLD("GoldMedium"),
    PLATINUM("PlatinumMedium"),
    DIAMOND23("Diamond-2-3"),
    DIAMOND("DiamondMedium"),
    MASTER("MasterMedium");

    private final String hrefPart;

    League(String hrefPart) {
        this.hrefPart = hrefPart;
    }

    public static Optional<League> parse(String href) {
        for (League league : League.values()) {
            if (href.contains(league.hrefPart)) {
                return Optional.of(league);
            }
        }
        return Optional.empty();
    }

    public String getHrefPart() {
        return hrefPart;
    }
}
