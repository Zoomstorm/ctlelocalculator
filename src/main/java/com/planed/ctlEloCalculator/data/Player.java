package com.planed.ctlEloCalculator.data;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class Player {
    private String name;
    private League league;
    private Race race;
    @EqualsAndHashCode.Exclude
    private Double elo;
    @EqualsAndHashCode.Exclude
    private int points;
    private Team team;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<Match> matches;
    @Builder.Default
    private List<PlayerSeasonHonor> honors = new ArrayList<>();

    public void addPoints(int additionalPoints) {
        points += additionalPoints;
    }

    public boolean wasActiveInSeason(int season) {
        return matches.stream()
                .filter(match -> match.getSeason() == season)
                .findAny().isPresent();
    }

    public void addHonor(PlayerSeasonHonor honor) {
        this.honors.add(honor);
    }

    public String getLiquiName() {
        return name.trim().replace(" ","_");
    }
}
