package com.planed.ctlEloCalculator.data.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "ARTICLE")
public class ArticleEntity {
    @Id
    @Column(unique = true)
    private String articleName;
    @Column
    private String hashBase64;
    @Column
    private OffsetDateTime hashValueLastUpdate;
    @Column
    private OffsetDateTime dataLastUpdate;
}
