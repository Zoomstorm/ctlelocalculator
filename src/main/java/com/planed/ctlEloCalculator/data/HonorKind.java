package com.planed.ctlEloCalculator.data;

import lombok.Getter;

@Getter
public enum HonorKind {
    FIVE_SET_WINS_IN_SEASON("5 Set wins in a season", "Star.png", "5 Set wins in season"),
    FIRST_PLACE_IN_SEASON("First place in a CTL Season", "Gold.png", "First place in season"),
    SECOND_PLACE_IN_SEASON("Second place in a CTL Season", "Silver.png", "Second place in season"),
    THIRD_PLACE_IN_SEASON("Third place in a CTL Season", "Bronze.png", "Third place in season");

    private final String title;
    private final String imageFile;
    private final String description;

    HonorKind(String title, String imageFile, String description) {
        this.title = title;
        this.imageFile = imageFile;
        this.description = description;
    }
}
