package com.planed.ctlEloCalculator.data;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class ChoboLeagueData {
    private int season;
    private Map<String, Team> teams;
    private Map<String, Player> players;
    private Map<String, Sc2Map> maps;
    private List<Match> matches;
    private Map<Integer, Boolean> seasonFinishedTable;

    public int getCurrentSeason() {
        return matches.stream()
                .mapToInt(Match::getSeason)
                .max().orElseThrow(()->new RuntimeException("Could not determine current season!"));
    }
}
