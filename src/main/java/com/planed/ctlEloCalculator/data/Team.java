package com.planed.ctlEloCalculator.data;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

@Data
@Builder
public class Team {
    private String name;
    @EqualsAndHashCode.Exclude
    private String iconUrl;
    @Builder.Default
    private Map<Integer, Integer> seasonPlacements = new HashMap<>();

    public String getLiquiName() {
        return name.trim().replace(" ", "_");
    }

    public String getPictureNameAndLinkString() {
        return "[[User:Fustup/Team/" + getLiquiName() + "|" + getName() + "]]" + "[[File:" + iconUrl + "]]";
    }

    public String getPictureWithLink() {
        return "[[Image:" + getIconUrl() + "|link=User:Fustup/Team/" + getLiquiName() + "|"+getName()+"]]";
    }
}
