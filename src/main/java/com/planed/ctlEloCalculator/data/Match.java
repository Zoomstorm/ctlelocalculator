package com.planed.ctlEloCalculator.data;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Match {
    private boolean playoffMatch;
    private boolean aceMatch;
    private Sc2Map map;
    private int season;
    private int week;
    private League league;
    private Player player1;
    private Player player2;
    private Team player1Team;
    private Team player2Team;
    private boolean player1Wins;
    private boolean player2Wins;
    private int player1Score;
    private int player2Score;
    private boolean isWalkover;

    public void addPlayer1Score(int additionalScore) {
        player1Score += additionalScore;
    }

    public void addPlayer2Score(int additionalScore) {
        player2Score += additionalScore;
    }

    public void calculateWinner() {
        if (player1Score > player2Score) {
            player1Wins = true;
            player2Wins = false;
        } else if (player1Score == player2Score) {
            player1Wins = false;
            player2Wins = false;
        } else {
            player1Wins = false;
            player2Wins = true;
        }
    }

    public boolean didPlayerWin(Player player) {
        return (player1 != null && player1Wins && player1.equals(player))
                || (player2 != null && player2Wins && player2.equals(player));
    }

    public int mapToPoints(Player player) {
        if (didPlayerWin(player)) {
            if (!isWalkover) {
                return 3;
            } else {
                return 2;
            }
        } else {
            if (!isWalkover) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public double getEloAdjustmentForPlayer(Player player) {
        if (isWalkover() || player == null) {
            return 0.;
        }

        double weightedEloDifference = (getPlayer2().getElo() - getPlayer1().getElo()) / 400.;
        double expectedOutcome = 1. / (1 + Math.pow(10., weightedEloDifference));

        if (player.equals(player1)) {
            return 20. * (getPlayer1Score() - expectedOutcome);
        } else if (player.equals(player2)) {
            return 20. * (getPlayer2Score() - (1. - expectedOutcome));
        } else {
            return 0.;
        }
    }

    public Team getTeamForPlayer(Player player) {
        if (player1 != null && player1.equals(player)) {
            return player1Team;
        } else if (player2 != null && player2.equals(player)) {
            return player2Team;
        } else {
            return null;
        }
    }
}
