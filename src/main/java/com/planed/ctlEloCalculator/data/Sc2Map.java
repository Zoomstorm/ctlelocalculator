package com.planed.ctlEloCalculator.data;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class Sc2Map {
    private String mapName;
    @Builder.Default
    @ToString.Exclude
    private List<Match> matches = new ArrayList<>();
}

