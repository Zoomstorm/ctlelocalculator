package com.planed.ctlEloCalculator.data.repositories;

import com.planed.ctlEloCalculator.data.entities.ArticleEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ArticleRepository extends CrudRepository<ArticleEntity, Long> {
    Optional<ArticleEntity> findByArticleName(String name);
}
