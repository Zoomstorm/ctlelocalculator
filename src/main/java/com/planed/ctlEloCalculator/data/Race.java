package com.planed.ctlEloCalculator.data;

import java.util.Optional;

public enum Race {
    ZERG("Zerg","Z"),
    TERRAN("Terran","T"),
    PROTOSS("Protoss","P"),
    RANDOM("Random","R");

    private final String title;
    private final String liquiString;

    Race(String title, String liquiString) {
        this.title = title;
        this.liquiString = liquiString;
    }

    public static Optional<Race> parse(String title) {
        for (Race race : Race.values()) {
            if (title.contains(race.title)) {
                return Optional.of(race);
            }
        }
        return Optional.empty();
    }

    public String toLiquipediaString() {
        return liquiString;
    }
}
