package com.planed.ctlEloCalculator.data;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PlayerSeasonHonor {
    private HonorKind kind;
    private int season;

    public String getPicture() {
        return "[[File:" + getKind().getImageFile() + "| " + getKind().getDescription() + " " + getSeason() + "]]";
    }
}
