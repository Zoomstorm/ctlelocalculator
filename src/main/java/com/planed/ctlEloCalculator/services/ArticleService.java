package com.planed.ctlEloCalculator.services;

import com.planed.ctlEloCalculator.data.entities.ArticleEntity;
import com.planed.ctlEloCalculator.data.repositories.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ArticleService {
    @Autowired
    private ArticleRepository articleRepository;

    public Optional<ArticleEntity> getArticleByName(String name) {
        return articleRepository.findByArticleName(name);
    }

    public void saveArticle(ArticleEntity build) {
        articleRepository.save(build);
    }
}
