package com.planed.ctlEloCalculator.services;

import com.planed.ctlEloCalculator.data.*;
import com.planed.ctlEloCalculator.utils.LiquipediaService;
import lombok.Data;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class LiquipediaMainTableExporter {
    private static final String MAIN_TABLE_HEADER = "==Player Table==\n" +
            "{|class=\"wikitable sortable\"\n" +
            "!colspan=15|Current Standing\n" +
            "|-\n" +
            "! #<!-- Rank -->\n" +
            "! ID\n" +
            "! ELO\n" +
            "! style=\"width: 150px;\" | Team\n" +
            "! data-sort-type=\"number\"| w\n" +
            "! data-sort-type=\"number\"| l\n" +
            "! data-sort-type=\"number\"| Σ\n" +
            "! data-sort-type=\"number\"| %\n" +
            "! data-sort-type=\"number\"| Points \n" +
            "!width=150px | Seasons Played\n" +
            "! Awards\n" +
            "!width=25px | Active\n\n";
    private static final String SEASON_TABLE_HEADER = "==Player Table==\n" +
            "{|class=\"wikitable sortable\"\n" +
            "!colspan=15|Current Standing\n" +
            "|-\n" +
            "! #<!-- Rank -->\n" +
            "! ID\n" +
            "! ELO\n" +
            "! style=\"width: 150px;\" | Team\n" +
            "! data-sort-type=\"number\"| w\n" +
            "! data-sort-type=\"number\"| l\n" +
            "! data-sort-type=\"number\"| Σ\n" +
            "! data-sort-type=\"number\"| %\n" +
            "! data-sort-type=\"number\"| Points \n" +
            "! Awards\n\n";
    private static final Integer CURRENT_SEASON = 21;
    private static final String ARTICLE_PREFIX = "User:Fustup/";

    @Autowired
    private LiquipediaService liquipediaService;

    public void uploadTable(String docName, ChoboLeagueData data) {
        MainTableExporterExecution exporterExecution = new MainTableExporterExecution();
        exporterExecution.setData(data);
        exporterExecution.setMainDocumentName(docName);
        exporterExecution.execute();
    }

    @Data
    private class MainTableExporterExecution {
        private String mainDocumentName;
        private ChoboLeagueData data;
        private Map<String, MultiValueMap<Integer, Match>> playerToMatchesBySeasonMap;

        public void execute() {
            sortMatches();

            writeMainArticle();
            writePlayerTableArticle();
            writePlayerStatisticsArticle();
            writeSeasonArticles();
        }

        private void sortMatches() {
            playerToMatchesBySeasonMap = new HashMap<>();
            data.getPlayers().values().stream()
                    .forEach(player -> {
                        MultiValueMap<Integer, Match> matchesBySeason = new LinkedMultiValueMap();
                        player.getMatches().stream()
                                .forEach(match -> matchesBySeason.add(match.getSeason(), match));
                        playerToMatchesBySeasonMap.put(player.getName(), matchesBySeason);
                    });
        }

        private String writeTabIntro(int selfTabNumber) {
            return "{{tabs static\n" +
                    "|name1=Overview\n" +
                    "|link1=" + ARTICLE_PREFIX + mainDocumentName + "\n" +
                    "|name2=Player Table\n" +
                    "|link2=" + ARTICLE_PREFIX + mainDocumentName + "/PlayerTable\n" +
                    "|name3=Statistics\n" +
                    "|link3=" + ARTICLE_PREFIX + mainDocumentName + "/Statistics\n" +
                    data.getSeasonFinishedTable().keySet().stream()
                            .map(season ->
                                    // why no +/- at season? because we start exactly with season #4
                                    "|name" + season + "=Season" + season + "\n" +
                                            "|link" + season + "=" + ARTICLE_PREFIX + mainDocumentName + "/Season" + season + "\n")
                            .collect(Collectors.joining("")) +
                    "|This=" + selfTabNumber + "}}\n\n";
        }

        private void writePlayerTableArticle() {
            liquipediaService.writeArticleWithTextIfDiffsPresent(
                    ARTICLE_PREFIX + mainDocumentName + "/PlayerTable",
                    writeTabIntro(2) + writePlayerTable());
        }

        private void writeMainArticle() {
            liquipediaService.writeArticleWithTextIfDiffsPresent(
                    "User:Fustup/" + mainDocumentName,
                    convertDataToWikiArticle(data));
        }

        private void writePlayerStatisticsArticle() {
            liquipediaService.writeArticleWithTextIfDiffsPresent(
                    "User:Fustup/" + mainDocumentName + "/Statistics",
                    writeTabIntro(3) + racialDistributionStatistics());
        }

        private void writeSeasonArticles() {
            data.getSeasonFinishedTable().keySet().stream()
                    // .filter(season -> season == 20)
                    .forEach(season -> writeSeasonArticle(season));
        }

        private void writeSeasonArticle(Integer season) {
            String seasonArticleContent = writeTabIntro(season);
            seasonArticleContent += "=Season " + season + "=\n\n";
            seasonArticleContent += SEASON_TABLE_HEADER;
            List<Pair<Double, Player>> eloPlayerList = data.getPlayers().values().stream()
                    .map(player -> getEloForPlayerInSeason(player, season))
                    .sorted(Comparator.comparing(pair -> -pair.getKey()))
                    .collect(Collectors.toList());
            int rank = 1;
            for (Pair<Double, Player> playerPair : eloPlayerList) {
                seasonArticleContent += playerToTableLine(playerPair.getRight(), rank++, season, playerPair.getLeft());
            }
            liquipediaService.writeArticleWithTextIfDiffsPresent(
                    "User:Fustup/" + mainDocumentName + "/Season" + season,
                    seasonArticleContent);
        }

        private String convertDataToWikiArticle(ChoboLeagueData data) {
            String result = writeTabIntro(1) + "=Overview of CTL=";

            return result;
        }

        private String writePlayerTable() {
            String result = MAIN_TABLE_HEADER;
            int rank = 1;
            for (Player player : data.getPlayers().values()
                    .stream()
                    .sorted(Comparator.comparing(player -> -player.getElo()))
                    .collect(Collectors.toList())) {
                result += playerToTableLine(player, rank++) + "\n";
            }
            result += "|}\n\n";
            return result;
        }

        private String racialDistributionStatistics() {
            String result = "==Racial Distribution==\n";
            boolean first = true;
            for (int season = 4; season < 30; season++) {
                final int s = season;
                List<Player> activePlayers = data.getPlayers().values().stream()
                        .filter(player -> player.wasActiveInSeason(s))
                        .collect(Collectors.toList());
                if (activePlayers.isEmpty()) {
                    continue;
                }
                if (!first) {
                    result += "}}\n";
                }

                result += "{{RaceDist|title=Season " + season + " | ";
                result += "protoss=" + getCount(activePlayers, Race.PROTOSS) + " |";
                result += "terran=" + getCount(activePlayers, Race.TERRAN) + " |";
                result += "zerg=" + getCount(activePlayers, Race.ZERG) + " |";
                result += "random=" + getCount(activePlayers, Race.RANDOM);
                if (first) {
                    result += "|first=1";
                    first = false;
                }
            }
            result += "|last=1}}\n";
            return result;
        }

        private long getCount(List<Player> activePlayers, Race protoss) {
            return activePlayers.stream()
                    .filter(player -> player.getRace() == protoss)
                    .count();
        }

        private String playerToTableLine(Player player, int rank) {
            long won = player.getMatches().stream()
                    .filter(match -> match.didPlayerWin(player))
                    .count();
            long points = player.getPoints();
            long lost = player.getMatches().size() - won;
            int winPercent = (int) (won / ((double) won + lost) * 100);

            String result = "|- ";
            if (playerToMatchesBySeasonMap.get(player.getName()).containsKey(CURRENT_SEASON)) {
                result += "style=\"background: #E3E7FF;\"";
            }
            result += "\n|" + rank + "|| {{nowrap| {{";
            if (player.getRace() == null) {
                result += "R";
            } else {
                result += player.getRace().toLiquipediaString();
            }
            result += "}} [[File:" + player.getLeague().getHrefPart() + ".png|17px]] [[User:Fustup/Player/" + player.getLiquiName() + "|" + player.getName();
            result += "]]}}||" + player.getElo().intValue() + "|| " + player.getTeam().getPictureWithLink() + "|| +" + won + "|| -" + lost + "|| Σ" + (won + lost) + "||" + winPercent + "%|| ";
            result += points + " pts || {{nowrap| ";
            result += playerToMatchesBySeasonMap.get(player.getName()).keySet().stream().map(s -> s.toString()).collect(Collectors.joining(", "));
            result += "}} || data-sort-value=\"" + player.getHonors().size() + "\" | ";
            result += player.getHonors().stream()
                    .map(honor -> honor.getPicture())
                    .collect(Collectors.joining(" ")) + " || ";
            if (playerToMatchesBySeasonMap.get(player.getName()).containsKey(CURRENT_SEASON)) {
                result += " x \n";
            }
            return result;
        }

        private Pair<Double, Player> getEloForPlayerInSeason(Player player, Integer season) {
            List<Match> matches = playerToMatchesBySeasonMap.get(player.getName()).getOrDefault(season, Collections.emptyList());
            double elo = 1200.;
            for (Match match : matches) {
                elo += match.getEloAdjustmentForPlayer(player);
            }
            return Pair.of(elo, player);
        }

        private String playerToTableLine(Player player, int rank, int season, double elo) {
            List<Match> matches = playerToMatchesBySeasonMap.get(player.getName()).get(season);
            if (matches == null || matches.isEmpty()) {
                return "";
            }
            Team team = matches.get(0).getTeamForPlayer(player);

            long won = matches.stream()
                    .filter(match -> match.didPlayerWin(player))
                    .count();
            long points = matches.stream()
                    .mapToInt(match -> match.mapToPoints(player))
                    .sum();
            long lost = matches.size() - won;
            int winPercent = (int) (won / ((double) won + lost) * 100);

            String result = "|- ";
            result += "\n|" + rank + "|| {{nowrap| {{";
            if (player.getRace() == null) {
                result += "R";
            } else {
                result += player.getRace().toLiquipediaString();
            }
            result += "}} [[File:" + player.getLeague().getHrefPart() + ".png|17px]] [[User:Fustup/Player/" + player.getLiquiName() + "|" + player.getName();
            result += "]]}}||" + (int) elo + "|| " + team.getPictureWithLink() + "|| +" + won + "|| -" + lost + "|| Σ" + (won + lost) + "||" + winPercent + "%|| ";
            result += points + " pts || data-sort-value=\"" + player.getHonors().stream()
                    .filter(honor -> honor.getSeason() == season).count() + "\" | ";
            result += player.getHonors().stream()
                    .filter(honor -> honor.getSeason() == season)
                    .map(honor -> honor.getPicture())
                    .collect(Collectors.joining(" ")) + " || ";
            return result + "\n";
        }
    }
}
