package com.planed.ctlEloCalculator.services;

import com.google.common.net.UrlEscapers;
import com.planed.ctlEloCalculator.data.*;
import com.planed.ctlEloCalculator.utils.LiquipediaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class LiquipediaUserPageExporter {
    private static final Logger logger = LoggerFactory.getLogger(LiquipediaUserPageExporter.class);

    @Autowired
    private LiquipediaService liquipediaService;

    public void uploadPlayer(Player player) {
        try {
            liquipediaService.writeArticleWithTextIfDiffsPresent(
                    "User:Fustup/Player/" + UrlEscapers.urlPathSegmentEscaper().escape(player.getLiquiName()),
                    convertPlayerToWikiArticle(player));
        } catch (Exception e) {
            logger.warn("Error while saving player " + player.getName() + ": ");
            e.printStackTrace();
        }
    }

    private String convertPlayerToWikiArticle(Player player) {
        return buildIntro(player)
                + buildInfoText(player)
                + "\n==Stats==\n" + buildStatsTable(player, player.getMatches())
                + buildSeasonList(player)
                + buildMatchTable(player);
    }

    private String buildSeasonList(Player player) {
        Set<Integer> participatedSeasons = new HashSet<>();
        MultiValueMap<Integer, PlayerSeasonHonor> honorsPerSeason = new LinkedMultiValueMap<>();
        for (PlayerSeasonHonor honor : player.getHonors()) {
            honorsPerSeason.add(honor.getSeason(), honor);
            participatedSeasons.add(honor.getSeason());
        }
        MultiValueMap<Integer, Match> matchesPerSeason = new LinkedMultiValueMap<>();
        for (Match match : player.getMatches()) {
            matchesPerSeason.add(match.getSeason(), match);
            participatedSeasons.add(match.getSeason());
        }

        return "\n" +
                "==Seasons==\n" +
                participatedSeasons.stream()
                        .sorted(Comparator.naturalOrder())
                        .map(season -> buildSeasonInfoString(season, honorsPerSeason.getOrDefault(season, Collections.emptyList()), matchesPerSeason.getOrDefault(season, Collections.emptyList()), player))
                        .collect(Collectors.joining("\n\n"));
    }

    private String buildSeasonInfoString(Integer season, List<PlayerSeasonHonor> playerSeasonHonors, List<Match> matches, Player player) {
        return "===Season " + season + "===\n" + buildStatsTable(player, matches) + buildHonorString(playerSeasonHonors) + "\n";
    }

    private String buildHonorString(List<PlayerSeasonHonor> playerSeasonHonors) {
        if (playerSeasonHonors.isEmpty()) {
            return "";
        }
        return "Awards: " + playerSeasonHonors.stream()
                .map(PlayerSeasonHonor::getPicture)
                .collect(Collectors.joining(" "));
    }

    private String buildStatsTable(Player player, List<Match> matches) {
        return "{|class=\"wikitable sortable\"\n" +
                "|-\n" +
                "|ELO||" + calculateEloForPlayer(player, matches) + "\n" +
                "|-\n" +
                "|Points||" + calculatePointsForPlayer(player, matches) + "\n" +
                "|-\n" +
                "|Matches Played||" + formatPercentWinLossString(matches, player) + "\n" +
                "|-\n" +
                "|Playoff Matches||" + formatPercentWinLossString(matches, player, Match::isPlayoffMatch) + "\n" +
                "|-\n" +
                "|Ace Matches||" + formatPercentWinLossString(matches, player, Match::isAceMatch) + "\n" +
                "|-\n" +
                "|Matches vs T||" + getWinrate(player, Race.TERRAN, matches) + "\n" +
                "|-\n" +
                "|Matches vs Z||" + getWinrate(player, Race.ZERG, matches) + "\n" +
                "|-\n" +
                "|Matches vs P||" + getWinrate(player, Race.PROTOSS, matches) + "\n" +
                "\n" +
                "|}\n\n";
    }

    private int calculatePointsForPlayer(Player player, List<Match> matches) {
        return matches.stream()
                .mapToInt(match -> match.mapToPoints(player))
                .sum();
    }

    private Integer calculateEloForPlayer(Player player, List<Match> matches) {
        double elo = 1200.;
        for (Match match : matches) {
            elo += match.getEloAdjustmentForPlayer(player);
        }
        return (int) elo;
    }

    private String buildIntro(Player player) {
        return "{{Infobox player\n" +
                "|id                 = " + player.getName() + "\n" +
                "|race               = " + player.getRace().toLiquipediaString() + "\n" +
                "|team               = " + player.getTeam().getName() + "\n" +
                "|ELO                = " + player.getElo().intValue() + "\n" +
                "|Points             = " + player.getPoints() + "\n" +
                "|Number of Matches  = " + player.getMatches().size() + "\n" +
                "|history        = " + findAllTeams(player).stream()
                .map(team -> "[[File:" + team.getIconUrl() + "|" + team.getName() + "]]")
                .collect(Collectors.joining("\n")) +
                "}}\n\n";
    }

    private List<Team> findAllTeams(Player player) {
        List<Team> teams = new ArrayList<>();
        for (Match match : player.getMatches()) {
            Team currentTeam = player.equals(match.getPlayer1()) ? match.getPlayer1Team() : match.getPlayer2Team();
            if (teams.isEmpty() || !teams.get(teams.size() - 1).equals(currentTeam)) {
                teams.add(currentTeam);
            }
        }
        return teams;
    }

    private String buildInfoText(Player player) {
        return player.getName() + " is a StarCraft II player, currently playing for " + player.getTeam().getPictureNameAndLinkString() + ".\n\n";
    }

    private String buildMatchTable(Player player) {
        return "=Matches=\n" +
                "{|class=\"wikitable sortable\"\n" +
                "|-\n" +
                "!Season\n" +
                "!Week\n" +
                "!Team<!-- TeamA -->\n" +
                "!<!-- PlayerA -->\n" +
                "!vs<!-- vs -->\n" +
                "!<!-- PlayerB -->\n" +
                "!Team<!-- TeamB -->\n" +
                player.getMatches().stream()
                        .map(match -> buildMatchTableLine(match, player))
                        .collect(Collectors.joining("\n")) +
                "|}";
    }

    private String buildMatchTableLine(Match match, Player player) {
        final boolean areWePlayer1 = player.equals(match.getPlayer1());
        final Player opponent = areWePlayer1 ? match.getPlayer2() : match.getPlayer1();
        String result = "";

        if (match.didPlayerWin(player)) {
            result += "|-style=\"background: #B8F2B8;\"\n";
        } else {
            result += "|-style=\"background: #F2B8B8;\"\n";
        }
        result += "| style=\"text-align: center;\" | " + match.getSeason() + "|| style=\"text-align: center;\" | ";
        if (match.isPlayoffMatch()) {
            result += match.getWeek() + " (Playoffs)";
        } else {
            result += match.getWeek();
        }
        result += "|| style=\"text-align: center;\" | " + (areWePlayer1 ? match.getPlayer1Team().getPictureNameAndLinkString() : match.getPlayer2Team().getPictureNameAndLinkString());

        result += matchTablePlayers1Fields(player, match);

        result += "|| style=\"text-align: center; font-weight: bold;\"| " + match.getPlayer1Score() + " [[File:" + match.getLeague().getHrefPart() + ".png|17px]] ";
        if (opponent != null) {
            result += matchTablePlayers2Fields(opponent, match);
        } else {
            result += "|| ";
        }
        result += "|| style=\"text-align: center;\" | " + (areWePlayer1 ? match.getPlayer2Team().getPictureNameAndLinkString() : match.getPlayer1Team().getPictureNameAndLinkString());
        return result + "\n";
    }

    private String matchTablePlayers1Fields(Player player, Match match) {
        boolean crossOutPlayer = !match.didPlayerWin(player) && match.isWalkover();
        final String nameString = (crossOutPlayer ? "<s>" : "") + player.getName() + (crossOutPlayer ? "</s>" : "");
        return " ||[[User:Fustup/Player/" + player.getLiquiName() + "|" + nameString + "]] {{" + player.getRace().toLiquipediaString() + "}} ''+" + match.mapToPoints(player) + "'' ";
    }

    private String matchTablePlayers2Fields(Player player, Match match) {
        boolean crossOutPlayer = !match.didPlayerWin(player) && match.isWalkover();
        final String nameString = (crossOutPlayer ? "<s>" : "") + player.getName() + (crossOutPlayer ? "</s>" : "");
        return match.getPlayer2Score() + " ||{{" + player.getRace().toLiquipediaString() + "}}[[User:Fustup/Player/" + player.getLiquiName() + "|" + nameString + "]]  ''+" + match.mapToPoints(player) + "'' ";
    }

    private String getWinrate(Player player, Race race, List<Match> matches) {
        return formatPercentWinLossString(matches, player, match -> getOpponent(match, player)
                .map(opponent -> opponent.getRace() == race)
                .orElse(false));
    }

    private String formatPercentWinLossString(List<Match> matches, Player player, Predicate<Match> predicate) {
        return formatPercentWinLossString(matches.stream()
                .filter(predicate)
                .collect(Collectors.toList()), player);
    }

    private String formatPercentWinLossString(List<Match> matches, Player player) {
        final long wonMatches = matches.stream()
                .filter(match -> match.didPlayerWin(player))
                .count();
        return matches.size() + ", +" + wonMatches + ", -" + (matches.size() - wonMatches) + ", winrate: " + (int) (wonMatches / (double) matches.size() * 100)
                + "%";
    }

    private Optional<Player> getOpponent(Match match, Player player) {
        if (match.getPlayer1() == null || match.getPlayer2() == null) {
            return Optional.empty();
        }
        if (match.getPlayer1().equals(player)) {
            return Optional.of(match.getPlayer2());
        } else {
            return Optional.of(match.getPlayer1());
        }
    }
}
