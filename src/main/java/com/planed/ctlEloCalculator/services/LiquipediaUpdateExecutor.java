package com.planed.ctlEloCalculator.services;

import com.planed.ctlEloCalculator.data.ChoboLeagueData;
import com.planed.ctlEloCalculator.data.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class LiquipediaUpdateExecutor {
    private static final Logger logger = LoggerFactory.getLogger(LiquipediaUpdateExecutor.class);

    @Autowired
    private LiquipediaImporter liquipediaImporter;
    @Autowired
    private LiquipediaMainTableExporter liquipediaMainTableExporter;
    @Autowired
    private LiquipediaUserPageExporter liquipediaUserPageExporter;
    @Autowired
    private LiquipediaTeamPageExporter liquipediaTeamPageExporter;
    @Autowired
    private ApplicationContext context;

    @PostConstruct
    public void triggerUpdate() {
        final ChoboLeagueData data = liquipediaImporter.importData();

        liquipediaMainTableExporter.uploadTable("CtlTable2", data);

        data.getTeams().values().stream()
                .forEach(team -> liquipediaTeamPageExporter.uploadTeam(team, data));

        final List<Player> values = new ArrayList<>(data.getPlayers().values());
        Collections.shuffle(values);
        int i = 0;
        long t1 = System.currentTimeMillis();
        for (Player p : values) {
            liquipediaUserPageExporter.uploadPlayer(p);
            if ((i++ % 10) == 0) {
                Duration elapsed = Duration.ofMillis(System.currentTimeMillis() - t1);
                double fraction = i / (double) values.size();
                double msPerArticle = (elapsed.toMillis() / ((double) i));
                Duration remain = Duration.ofMillis((long) ((values.size() - i) * msPerArticle));
                logger.info("Checked " + i + " items of " + values.size() + ", being " + fraction * 100 + "%. Elapsed: " + elapsed + ", remaining: " + remain);
            }
        }

        ((ConfigurableApplicationContext) context).close();
    }
}
