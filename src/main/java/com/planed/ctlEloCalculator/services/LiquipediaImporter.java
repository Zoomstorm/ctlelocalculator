package com.planed.ctlEloCalculator.services;

import com.planed.ctlEloCalculator.data.ChoboLeagueData;
import com.planed.ctlEloCalculator.executor.LiquipediaImporterExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;

@Service
public class LiquipediaImporter {
    @Autowired
    private LiquipediaReader liquipediaReader;

    public ChoboLeagueData importData() {
        ChoboLeagueData data = ChoboLeagueData.builder()
                .players(new HashMap<>())
                .matches(new ArrayList<>())
                .teams(new HashMap<>())
                .maps(new HashMap<>())
                .seasonFinishedTable(new HashMap<>())
                .build();

        for (int season = 4; season < 40; season++) {
            data = LiquipediaImporterExecutor.builder()
                    .htmlData(liquipediaReader.readSeasonFiles(season))
                    .season(season)
                    .choboLeagueData(data)
                    .build()
                    .execute();
            if (!data.getSeasonFinishedTable().get(season)) {
                break;
            }
        }

        return data;
    }
}
