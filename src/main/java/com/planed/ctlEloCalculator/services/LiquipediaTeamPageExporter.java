package com.planed.ctlEloCalculator.services;

import com.google.common.net.UrlEscapers;
import com.planed.ctlEloCalculator.data.*;
import com.planed.ctlEloCalculator.utils.LiquipediaService;
import lombok.Builder;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LiquipediaTeamPageExporter {
    private static final Logger logger = LoggerFactory.getLogger(LiquipediaTeamPageExporter.class);

    @Autowired
    private LiquipediaService liquipediaService;

    public void uploadTeam(Team team, ChoboLeagueData data) {
        try {
            LiquipediaTeamPageExporterExecutor.builder()
                    .team(team)
                    .data(data)
                    .liquipediaService(liquipediaService)
                    .build()
                    .execute();
        } catch (Exception e) {
            logger.warn("Error while saving team " + team.getName() + ": " + ExceptionUtils.getStackTrace(e));
        }
    }

    @Builder
    private static class LiquipediaTeamPageExporterExecutor {
        private Team team;
        private ChoboLeagueData data;
        private List<Player> playerList;
        private List<Integer> seasonsList;
        private LiquipediaService liquipediaService;

        public void execute() {
            playerList = data.getPlayers().values().stream()
                    .filter(player -> player.getMatches().stream()
                            .filter(match -> team.equals(match.getTeamForPlayer(player)))
                            .findAny().isPresent())
                    .collect(Collectors.toList());

            seasonsList = data.getMatches().stream()
                    .filter(match -> team.equals(match.getPlayer1Team()) || team.equals(match.getPlayer2Team()))
                    .map(Match::getSeason)
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());

            liquipediaService.writeArticleWithTextIfDiffsPresent(
                    "User:Fustup/Team/" + UrlEscapers.urlPathSegmentEscaper().escape(team.getLiquiName()),
                    convertTeamToWikiArticle());
        }

        private String convertTeamToWikiArticle() {
            return getInfoBox() + getDescriptionText() + getTeamAchievements() + getPlayerTable();
        }

        private String getTeamAchievements() {
            return "==Team Achievements==\n" +
                    "{| class=\"sortable wikitable\"\n" +
                    "!width=150px| Chobo Teamleague Season\n" +
                    "!width=220px | Result\n" +
                    "\n" +
                    seasonsList.stream()
                            .map(season -> mapSeasonToTableLine(season))
                            .collect(Collectors.joining("\n")) +
                    "|}\n\n";
        }

        private String mapSeasonToTableLine(Integer season) {
            return "|-\n" +
                    "| Season " + season + "\n" +
                    "| " + mapPlacementToLiquiString(team.getSeasonPlacements().get(season)) + "\n" +
                    "\n";

        }

        private String mapPlacementToLiquiString(Integer placement) {
            if (placement == null) {
                return "";
            } else if (placement == 1) {
                return "{{Medal|1st}} First place";
            } else if (placement == 2) {
                return "{{Medal|2nd}} Second place";
            } else if (placement == 3) {
                return "{{Medal|3rd}} Third place";
            } else if (placement == 4) {
                return "{{Medal|4th}} Fourth place";
            } else {
                return "";
            }
        }

        private String getPlayerTable() {
            return "==Players==\n" +
                    "{|class=\"sortable wikitable\"\n" +
                    "! width=\"175\" | Name\n" +
                    "! width=\"175\" | Games played for team\n" +
                    "! width=\"175\" | Seasons participated\n" +
                    "! width=\"75\" | Active\n" +
                    playerList.stream()
                            .map(player -> playerToTableLine(player))
                            .collect(Collectors.joining("\n")) +
                    "\n|}\n\n";
        }

        private String playerToTableLine(Player player) {
            final List<Match> matchesForTeam = player.getMatches().stream()
                    .filter(match -> team.equals(match.getTeamForPlayer(player)))
                    .collect(Collectors.toList());
            final List<Integer> seasonsList = matchesForTeam.stream()
                    .map(Match::getSeason)
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());
            final String raceString = player.getRace() == null ? "" : player.getRace().toLiquipediaString();
            String styleString = seasonsList.contains(data.getCurrentSeason()) ? "style=\"background: #E3E7FF;\"" : "";
            return "|-" + styleString + "\n|{{" + raceString + "}} [[User:Fustup/Player/" + player.getLiquiName() + "|" + player.getName() + "]]\n" +
                    "|data-sort-value=\"" + matchesForTeam.size() + "\" | " + formatPercentWinLossString(matchesForTeam, player) + "\n" +
                    "|data-sort-value=\"" + seasonsList.size() + "\" | " + seasonsList
                    .stream()
                    .map(season -> season.toString())
                    .collect(Collectors.joining(", ")) + "\n" +
                    "|" + seasonsList.stream()
                    .filter(season -> data.getCurrentSeason() == season.intValue())
                    .map(s -> "x")
                    .findAny().orElse("");
        }

        private String getDescriptionText() {
            return team.getName() + " is a team competing in the Chobo Team League.\n\n";
        }

        private String getInfoBox() {
            final int lastSeason = data.getMatches().stream()
                    .filter(match -> team.equals(match.getPlayer1Team()) || team.equals(match.getPlayer2Team()))
                    .mapToInt(Match::getSeason)
                    .max().orElse(-1);
            String disbandedString = lastSeason == data.getCurrentSeason() ? "" : "|disbanded=Season " + lastSeason + "\n";
            return "{{Infobox team\n" +
                    "|name=" + team.getName() + "\n" +
                    "|image=" + team.getIconUrl() + "\n" +
                    "|player_number=" + playerList.size() + "\n" +
                    "|protoss_number=" + playerList.stream()
                    .filter(player -> player.getRace() == Race.PROTOSS)
                    .count() + "\n" +
                    "|terran_number=" + playerList.stream()
                    .filter(player -> player.getRace() == Race.TERRAN)
                    .count() + "\n" +
                    "|zerg_number=" + playerList.stream()
                    .filter(player -> player.getRace() == Race.ZERG)
                    .count() + "\n" +
                    "|created=Season " + seasonsList.get(0) + "\n" +
                    disbandedString +
                    "}}<br/>\n\n";
        }

        private String formatPercentWinLossString(List<Match> matches, Player player) {
            final long wonMatches = matches.stream()
                    .filter(match -> match.didPlayerWin(player))
                    .count();
            return matches.size() + ", +" + wonMatches + ", -" + (matches.size() - wonMatches) + ", winrate: " + (int) (wonMatches / (double) matches.size() * 100)
                    + "%";
        }
    }
}
