package com.planed.ctlEloCalculator.services;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.*;

@Service
public class LiquipediaReader {
    private static final Logger logger = LoggerFactory.getLogger(LiquipediaReader.class);
    private static String GROUP_STAGE_SUFFIX = "/Group_Stage";
    private static String PLAYOFF_SUFFIX = "/Playoffs";
    private Map<Integer, String> seasonMapping = new HashMap() {{
        put(21, "https://liquipedia.net/starcraft2/User:ChoboTeamLeague/Chobo_Team_League_Season_21");
        put(20, "https://liquipedia.net/starcraft2/User:ChoboTeamLeague/Chobo_Team_League_Season_20");
        put(19, "https://liquipedia.net/starcraft2/User:ChoboTeamLeague/Chobo_Team_League_Season_19");
        put(18, "https://liquipedia.net/starcraft2/User:ChoboTeamLeague/Chobo_Team_League_Season_18");
        put(17, "https://liquipedia.net/starcraft2/User:Tromboneham/Chobo_Team_League_Season_17");
        put(16, "https://liquipedia.net/starcraft2/User:Tromboneham/Chobo_Team_League_Season_16");
        put(15, "https://liquipedia.net/starcraft2/User:Tromboneham/Chobo_Team_League_Season_15");
        put(14, "https://liquipedia.net/starcraft2/User:Tromboneham/Chobo_Team_League_Season_14");
        put(13, "https://liquipedia.net/starcraft2/User:Tromboneham/Chobo_Team_League_Season_13");
        put(12, "https://liquipedia.net/starcraft2/User:Tromboneham/Chobo_Team_League_Season_12");
        put(11, "https://liquipedia.net/starcraft2/User:Tromboneham/Chobo_Team_League_Season_11");
        put(10, "https://liquipedia.net/starcraft2/User:Tromboneham/Chobo_Team_League_Season_10");
        put(9, "https://liquipedia.net/starcraft2/User:Tromboneham/Chobo_Team_League_Season_9");
        put(8, "https://liquipedia.net/starcraft2/User:Antylamon/Chobo_Team_League_Season_8");
        put(7, "https://liquipedia.net/starcraft2/User:Antylamon/Chobo_Team_League_Season_7");
        put(6, "https://liquipedia.net/starcraft2/User:Antylamon/Chobo_Team_League_Season_6");
        put(5, "https://liquipedia.net/starcraft2/User:Antylamon/Chobo_Team_League_Season_5");
        put(4, "https://liquipedia.net/starcraft2/User:Antylamon/Chobo_Team_League_Season_4");
    }};

    public List<String> readSeasonFiles(int... seasons) {
        List<String> data = new ArrayList<>();
        for (int season : seasons) {
            if (season == 11) {
                data.add(retrieveUrl(seasonMapping.get(season), season));
                data.add(retrieveUrl(seasonMapping.get(season) + GROUP_STAGE_SUFFIX + "_1", season));
                data.add(retrieveUrl(seasonMapping.get(season) + GROUP_STAGE_SUFFIX + "_2", season));
                data.add(retrieveUrl(seasonMapping.get(season) + PLAYOFF_SUFFIX, season));
            } else {
                data.add(retrieveUrl(seasonMapping.get(season), season));
                data.add(retrieveUrl(seasonMapping.get(season) + GROUP_STAGE_SUFFIX, season));
                data.add(retrieveUrl(seasonMapping.get(season) + PLAYOFF_SUFFIX, season));
            }
        }
        return data;
    }

    private String retrieveUrl(String url, int season) {
        try {
            logger.info("Reading URL '" + url + "'...");
            final HttpResponse<String> stringHttpResponse = Unirest.get(url).asString();
            if (stringHttpResponse.getStatus() >= 300) {
                throw new RuntimeException("Error while reading URL. Got response code '" + stringHttpResponse.getStatus() + "' for url '" + url + "'");
            }
//            final String suffix = !seasonMapping.containsValue(url) ? "_" + url.split("\\/")[url.split("\\/").length - 1] : "";
//            FileUtils.writeStringToFile(new File("src/test/resources/liquipedia/S" + season + suffix + ".html"), stringHttpResponse.getBody());
            try {
                Thread.sleep(2_000);
            } catch (InterruptedException e) {
            }
            return stringHttpResponse.getBody();
        } catch (UnirestException e) {
            throw new RuntimeException("Error while reading URL: ", e);
//        } catch (IOException e) {
//            throw new RuntimeException("Error while reading URL: ", e);
        }
    }
}
